<p align="center">
<img src="../logo.jpg" width="36"height="36"/>
</p>


## API Reference


### Historical data

Per convenience we are handing over the [JSON file](historic_data.json) with the response to our endpoint:

```json
{
   "building_active_power": 142.31273333333328,
   "grid_active_power": 89.78836666666669,
   "pv_active_power": 22.475533333333335,
   "quasars_active_power": -30.048833333333324,
   "timestamp": "2021-09-27T16:06:00+00:00"
}
```
Each object consist of a sample of data with the following sources of power:

| Parameters   |      Description      |  Units |
|----------|-------------|------|
|  `building_active_power` |    The building energy consumption in   |  kW |
|  `grid_active_power` |    The power consumed from the Grid.   |  kW |
|  `pv_active_power` |    The power from solar panels. |  kW |
|  `quasars_active_power` |    The power transfered to the system energy. If the value is positive the cars are being charged with energy from the grid. If the value is negative it means it is discharging to the building to supply the building demand.  |  kW |
|  `timestamp` |    An epoch timestamp of the sample of data. |  kW |



### Live data

Per convenience we are handing over the [JSON file](live_data.json) with the response to our endpoint:

```json
{
    "solar_power": 7.827,
    "quasars_power": -38.732,
    "grid_power": 80.475,
    "building_demand": 127.03399999999999,
    "system_soc": 48.333333333333336,
    "total_energy": 960,
    "current_energy": 464.0
}
```

| Parameters   |      Description      |  Units |
|----------|-------------|------|
| `solar_power` |   The power from solar panels   |   kW  |
|  `quasars_power` |    if the value is positive the cars are being charged with energy from the grid. If the value is negative it means it is discharging to the building to supply the building demand.   |  kW |
| `grid_power` |   The power consumed from the Grid.   |   kW  |
| `building_demand` |   The total power demand from the building   |   kW  |
| `system_soc` |    The system state of charge.   |   Expressed In % |
| `total_energy` |   TBD   |   kWh  |
| `current_energy` |   TBD   |   kWh  |

