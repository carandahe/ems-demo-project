<p align="center">
<img src="../logo.jpg" alt="drawing" width="36"height="36"/>
</p>

## What is a EMS?

The main goal of the Energy Management System is to improve energy efficiency in homes and buildings. It can combine different sources of energy like solar panels, wind turbines, stationary batteries, and the grid. Additional goals of an EMS may include electric utility benefits, such as controlling energy usage to reduce peak demand and support load shifting.
